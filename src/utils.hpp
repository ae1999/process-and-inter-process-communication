#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include "Def.hpp"

std::string trim(const std::string& line);
std::string get_string(std::stringstream& data);
float get_float(std::stringstream& data);
std::vector<std::string> process_command(std::string command);
std::vector<std::string> files_divider(int processes);

#endif