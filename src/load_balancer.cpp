#include "utils.hpp"

int load_balancer(int processes, std::vector<std::string> conditions, std::vector<std::string> files) {
	

	for(int i=0; i<processes; i++) {
		unlink((FIFO + std::to_string(i)).c_str());
		mkfifo((FIFO + std::to_string(i)).c_str(), 0666);
	}
	
	for(int i=0; i<processes; i++) {
		int linear_pipe[2];
		pipe(linear_pipe);
		int p = fork();
		if(p == 0) {
			close(linear_pipe[WRITE_END]);
			char input[BUF_SIZE];
			read(linear_pipe[READ_END], input, BUF_SIZE);
			close(linear_pipe[READ_END]);
			char *args[] = {"./worker", input, NULL};
			execv(args[0], args);
		}
		else if(p > 0) {
			close(linear_pipe[READ_END]);
			std::string output = std::to_string(i) + "&" + conditions[0] + "&" + files[i];
			write(linear_pipe[WRITE_END], output.c_str(), output.length() + 1);
			close(linear_pipe[WRITE_END]);
		}
	}

	int presenter_pipe[2];
	pipe(presenter_pipe);
    int presenter = fork();
    if (presenter == 0)
    {
        close(presenter_pipe[WRITE_END]);
        char input[BUF_SIZE];
        read(presenter_pipe[READ_END], input, BUF_SIZE);
        char *args[] = {"./presenter", input, NULL};
        execv(args[0], args);
        close(presenter_pipe[READ_END]);
    }
    else if (presenter > 0)
    {
        close(presenter_pipe[READ_END]);
        std::string msg = conditions[1] + "&" + conditions[2];
        write(presenter_pipe[WRITE_END], msg.c_str(), msg.length() + 1);
        close(presenter_pipe[WRITE_END]);
    }
	
	return 0;
}