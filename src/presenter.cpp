#include "Data_set.hpp"

std::vector<std::string> import_data(int workers) {
	std::vector<std::string> res;
	std::string result = "";
	for(int i=0; i<workers; i++) {
		std::ifstream file((FIFO + std::to_string(i)).c_str());
		std::string line;
		while(getline(file, line, '`'))
			result += line;
	}
	std::stringstream data2(result);
	std::string temp;
	while(std::getline(data2, temp, '$'))
		res.push_back(temp);
	return res;
}

int main(int argc, char *argv[]) {
	
	std::string conditions, _workers;
	std::stringstream data(argv[1]);
	std::getline(data, conditions, '&');
	std::getline(data, _workers, '&');
	int workers = std::stoi(_workers);

	std::vector<std::string> result = import_data(workers);

	Data_set *data_set = new Data_set;
	data_set->set_sorting_values(conditions);
	data_set->add_data(result);
	data_set->sort_data();
	data_set->present_data();
	
	return 0;
}