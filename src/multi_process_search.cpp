#include "load_balancer.cpp"

int main() {

	std::string command;

	while(getline(std::cin,command)) {
		if(command == QUIT)
			return 0;
		std::vector<std::string> conditions = process_command(command);
		int processes = std::stoi(conditions[2], nullptr, 10);
		std::vector<std::string> files = files_divider(processes);

		load_balancer(processes, conditions, files);
	}
	
	return 0;
}