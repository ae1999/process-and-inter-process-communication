#include "Game.hpp"

Game::Game(std::string game_data) {
	std::stringstream data(game_data);
	zip = game_data;
	name = get_string(data);
	platform = get_string(data);
	year = get_string(data);
	genre = get_string(data);
	publisher = get_string(data);
	NA_Sales = get_float(data);
	EU_Sales = get_float(data);
	JP_Sales = get_float(data);
	Other_Sales = get_float(data);
	Global_Sales = get_float(data);
}