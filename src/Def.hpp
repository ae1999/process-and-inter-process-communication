#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>
#include <sys/wait.h>
#include <sys/stat.h>
#include <bits/stdc++.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <algorithm>
#define header "Name - Platform - Year - Genre - Publisher - NA_Sales - EU_Sales - JP_Sales - Other_Sales - Global_Sales"
#define READ_END 0
#define WRITE_END 1
#define dataset "dataset"
#define FILE_NUMBERS 18
#define HEADER std::cout<<header<<std::endl
#define BUF_SIZE 1000
#define B_BUF_SIZE 10000
const std::string FIFO =  "myfifo";
const std::string END =  "`";
#define QUIT "quit"