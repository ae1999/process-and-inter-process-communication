#include "Data_set.hpp"

std::string Data_set::sorting_value;

void Data_set::read_file(std::string file_name) {
	std::ifstream file;
	file.open("../sales/"+file_name);
	std::string data;
	getline(file,data);
	while(getline(file,data)) {
		Game *new_game = new Game(data);
		games.push_back(new_game);
	}
	file.close();
}

void Data_set::read_files(std::string files) {
	std::stringstream data(files);
	std::string temp;
	while(data>>temp)
		read_file(temp);
}

std::vector<std::string> Data_set::process_conditions(std::string conditions) {
	while(conditions.find(" = ") != std::string::npos)
		conditions.replace(conditions.find(" = "), 3, "-");
	while(conditions.find(" - ") != std::string::npos)
		conditions.replace(conditions.find(" - "), 3, "-");
	std::vector<std::string> res = {"", "", "", "", ""};
	std::string attr[] = {"Name", "Platform", "Year", "Genre", "Publisher"};
	std::stringstream data(conditions);
	std::string temp, t;
	while(std::getline(data, temp, '-')) {
		for(int i=0; i<sizeof(attr); i++) {
			if(temp == attr[i]) {
				std::getline(data, t, '-');
				res[i] = trim(t);
				break;
			}
		}
	}
	return res;
}

std::vector<std::string> Data_set::search(std::string conditions) {
	std::vector <std::string> con = process_conditions(conditions);
	std::vector<std::string> res;
	for(int i=0; i<games.size(); i++)
		if((games[i]->get_name() == con[0] || con[0] == "") && 
			(games[i]->get_platform() == con[1] || con[1] == "") &&
			(games[i]->get_year() == con[2] || con[2] == "") &&
			(games[i]->get_genre() == con[3] || con[3] == "") &&
			(games[i]->get_publisher() == con[4] || con[4] == ""))
			res.push_back(games[i]->get_zip());
	return res;
}

void Data_set::set_sorting_values(std::string conditions) {
	std::stringstream data(conditions);
	data>>sorting_value;
	data>>a_d;
}

void Data_set::add_data(std::vector<std::string> data) {
	data.pop_back();
	for(int i=0; i<data.size(); i++) {
		Game *new_game = new Game(data[i]);
		games.push_back(new_game);
	}
}

bool Data_set::compare(Game *a, Game *b) {
	if(sorting_value == "Global_Sales")
		return a->get_Global_Sales() < b->get_Global_Sales();
	if(sorting_value == "EU_Sales")
		return a->get_EU_Sales() < b->get_EU_Sales();
	if(sorting_value == "JP_Sales")
		return a->get_JP_Sales() < b->get_JP_Sales();
	if(sorting_value == "Other_Sales")
		return a->get_Other_Sales() < b->get_Other_Sales();
	return a->get_NA_Sales() < b->get_NA_Sales();
}

void Data_set::sort_data() {
	std::sort(games.begin(), games.end(), compare);
}

void Data_set::present_data() {
	HEADER;
	if(a_d == "descending")
		for(int i=games.size()-1; i>=0; i--)
			std::cout<<i<<games[i]->get_zip()<<std::endl;
	else
		for(int i=0; i<games.size(); i++)
			std::cout<<games[i]->get_zip()<<std::endl;

}