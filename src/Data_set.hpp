#ifndef _DATA_SET_HPP_
#define _DATA_SET_HPP_

#include "Game.hpp"

class Data_set {
private:
	std::vector<Game *> games;
	static std::string sorting_value;
	std::string a_d;
public:
	void read_file(std::string file_name);
	void read_files(std::string files);
	std::vector<std::string> search(std::string conditions);
	std::vector<std::string> process_conditions(std::string conditions);
	void set_sorting_values(std::string conditions);
	void add_data(std::vector<std::string> data);
	void sort_data();
	void present_data();
	static bool compare(Game *a, Game *b);
};

#endif