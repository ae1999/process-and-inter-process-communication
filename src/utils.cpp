#include "utils.hpp"

std::string trim(const std::string& line) {
    const char* WhiteSpace = " \t\v\r\n";
    std::size_t start = line.find_first_not_of(WhiteSpace);
    std::size_t end = line.find_last_not_of(WhiteSpace);
    return start == end ? std::string() : line.substr(start, end - start + 1);
}

std::string get_string(std::stringstream& data) {
	std::string res;
	std::getline(data, res, '-');
	return trim(res);
}
float get_float(std::stringstream& data) {
	std::string res;
	std::getline(data, res, '-');
	if(res == "N/A")
		return -1;
	return std::stof(res);
}

std::vector<std::string> process_command(std::string command) {
	std::vector<std::string> res;
	std::string sort[] = {" NA_Sales", " EU_Sales", " JP_Sales", " Other_Sales", " Global_Sales"}; 
	for(int i=0; i<5; i++) {
		if(command.find(sort[i]) != std::string::npos) {
			res.push_back(command.substr(0,command.find(sort[i])));
			command = command.substr(command.find(sort[i])+1, std::string::npos);
		}
	}
	while(command.find("= ") != std::string::npos)
		command.replace(command.find("= "), 2, "");
	while(command.find("- ") != std::string::npos)
		command.replace(command.find("- "), 2, "");
	res.push_back(command.substr(0,command.find(" processes")));
	command = command.substr(command.find(" processes")+1, std::string::npos);
	res.push_back(command.substr(0, command.find(" dir")));
	res[2].replace(res[2].find("processes "), 10, "");
	res.push_back(command.substr(command.find(" dir")+1, std::string::npos));
	return res;
}

std::vector<std::string> files_divider(int processes) {
	std::vector<std::string> files;
	for(int i=0; i<processes; i++)
		files.push_back("");
	for(int i=1; i<FILE_NUMBERS; i+=processes)
		for(int j=0; j<processes; j++)
			if(i+j < FILE_NUMBERS)
				files[j] += dataset + std::to_string(i+j) + " ";
	return files;
}