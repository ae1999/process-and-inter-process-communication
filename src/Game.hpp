#ifndef _GAME_HPP_
#define _GAME_HPP_

#include "utils.hpp"

class Game {
private:
	std::string name;
	std::string platform;
	std::string year;
	std::string genre;
	std::string publisher;
	float NA_Sales;
	float EU_Sales;
	float JP_Sales;
	float Other_Sales;
	float Global_Sales;

	std::string zip;

public:
	Game(std::string game_data);
	std::string get_zip() { return zip; }
	std::string get_name() { return name; }
	std::string get_platform() { return platform; }
	std::string get_year() { return year; }
	std::string get_genre() { return genre; }
	std::string get_publisher() { return publisher; }
	float get_NA_Sales() { return NA_Sales; }
	float get_EU_Sales() { return EU_Sales; }
	float get_JP_Sales() { return JP_Sales; }
	float get_Other_Sales() { return Other_Sales; }
	float get_Global_Sales() { return Global_Sales; }
};

#endif