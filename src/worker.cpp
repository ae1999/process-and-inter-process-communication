#include "Data_set.hpp"

void export_data(std::vector<std::string> filtered, std::string worker_number) {
	std::string export_data = "";
	for(std::string s: filtered)
		export_data += s + '$';
	export_data += END;
	int fd = open((FIFO + worker_number).c_str(), O_WRONLY); 
	write(fd, export_data.c_str(), export_data.length() + 1);
	close(fd);
}

int main(int argc, char *argv[]) {

	std::string worker_number, conditions, files;
	std::stringstream data(argv[1]);
	std::getline(data, worker_number, '&');
	std::getline(data, conditions, '&');
	std::getline(data, files, '&');

	Data_set *data_set = new Data_set;
	data_set->read_files(files);
	std::vector<std::string> filtered = data_set->search(conditions);

	export_data(filtered, worker_number);
	
	return 0;
}